﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.ServiceProcess;

namespace Kura.HostService
{
    public partial class HostService : ServiceBase
    {
        public HostService()
        {
            InitializeComponent();
        }

        public static void Main(string[] args)
        {
            foreach (var s in args)
            {
                switch (s)
                {
                    // Check if this is running as a console app
                    case "--c":
                    case "--console":
                    {
                        new HostService().OnStart(null);
                        return;
                    }
                    
                    // install the service
                    case "--i":
                    case "--install":
                    {
                        MyServiceInstaller.Install(false, null);
                        return;
                    }

                    // uninstall the service
                    case "--u":
                    case "--uninstall":
                    {
                        MyServiceInstaller.Install(true, null);
                        return;
                    }
                }
                Console.WriteLine(@"Unknown argument " + s);
                Console.WriteLine(@"--c | --console => run as console");
                Console.WriteLine(@"--i | --install => install as service");
                Console.WriteLine(@"--u | --uninstall => uninstall service");
            }

            // If no arguments supplied, then this is running as a service, not a console app
            if (args.Any()) return;

            // Run as a service
            var servicesToRun = new ServiceBase[] 
            { 
                new HostService() 
            };
            Run(servicesToRun);
        }

        protected override void OnStart(string[] args)
        {
            var serviceStatus = new ServiceStatus
            {
                dwCurrentState = ServiceState.SERVICE_START_PENDING,
                dwWaitHint = 100000
            };
            SetServiceStatus(ServiceHandle, ref serviceStatus);

            //todo: start a new thread, etc....
            
            // Update the service state to Running.
            serviceStatus.dwCurrentState = ServiceState.SERVICE_RUNNING;
            SetServiceStatus(ServiceHandle, ref serviceStatus);
        }

        protected override void OnStop()
        {
            // Update the service state to Stop Pending.
            var serviceStatus = new ServiceStatus
            {
                dwCurrentState = ServiceState.SERVICE_STOP_PENDING,
                dwWaitHint = 100000
            };
            SetServiceStatus(ServiceHandle, ref serviceStatus);

            //todo: stop the thread here..

            // Update the service state to Stopped.
            serviceStatus.dwCurrentState = ServiceState.SERVICE_STOPPED;
            SetServiceStatus(ServiceHandle, ref serviceStatus);
        }


        // ADVAPI pinvoke stuff 

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(IntPtr handle, ref ServiceStatus serviceStatus);

        public enum ServiceState
        {
            SERVICE_STOPPED = 0x00000001,
            SERVICE_START_PENDING = 0x00000002,
            SERVICE_STOP_PENDING = 0x00000003,
            SERVICE_RUNNING = 0x00000004,
            SERVICE_CONTINUE_PENDING = 0x00000005,
            SERVICE_PAUSE_PENDING = 0x00000006,
            SERVICE_PAUSED = 0x00000007,
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct ServiceStatus
        {
            public long dwServiceType;
            public ServiceState dwCurrentState;
            public long dwControlsAccepted;
            public long dwWin32ExitCode;
            public long dwServiceSpecificExitCode;
            public long dwCheckPoint;
            public long dwWaitHint;
        };
    }
}
